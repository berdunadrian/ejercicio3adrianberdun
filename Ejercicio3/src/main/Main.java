package main;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner (System.in);
		System.out.println("Introduzca una frase: ");
		String cadena =  in.nextLine();
		
		String cadenaMayus = cadena.toUpperCase();
		
		System.out.println("La frase en may�sculas: "+cadenaMayus);
		in.close();
	}

}
